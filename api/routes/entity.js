var express = require('express');
var router = express.Router();
var entity = require('../models/entity');
var util = require('../util/util');

var self = {
	entity : {},
	list : []
};

router.get(util.api+'/entity', function (req, res, next) {
	var query = entity.getQuery(req.query);
	util.connection.query(query, function (error, results){
		if (error) {
	      return res.send(400, {'success':false,'data' : []});
	    }else{
	      if(results.length > 0){
	        var response = self.getList(results);
	        return res.send(200, {'success':true,'data' : response});
	      }else{
	        return res.send({'success':false,'data' : []});
	      }
	    }
	});
});

self.getList = function(data){
	if(data.length > 0){
		self.list = [];
		for (var i = 0; i < data.length; i++) {
			self.list.push({
		        'ruc' : data[i].ruc,
		        'name' : data[i].name,
		        'name_short' : data[i].name_short,
		        'number' : data[i].number,
		        'email' : data[i].email,
		        'flg_salud' : data[i].flg_salud,
		        'flg_educ' : data[i].flg_educ,
		        'number_length' : data[i].number_length,
		        'address' : data[i].address,
		        'data' : (data[i].data) ? JSON.parse(data[i].data) : {}
			});
		}
	}
	return self.list;
};

module.exports = router;