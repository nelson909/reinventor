var express = require('express');
var router = express.Router();

var doc = {
	query : '',
	queryDetail : '',
	insert : '',
	update : ''
};

doc.getQuery = function(params){
	doc.query = `SELECT d.id,d.number_document,d.amount,d.created_at
				 from ospos_document d
			     where d.deleted <> 1 `;
	if(params.hasOwnProperty('order_id')){
		doc.query += `and d.order_id = `+ params.order_id;
	}
	return doc.query;
};

doc.getQueryInsert = function(){
	doc.insert = `INSERT INTO ospos_document(number_document,order_id,amount) VALUES ? `;
	return doc.insert;
};

doc.getMaxNro = function(params){
	doc.query = '';
	doc.query = `SELECT MAX(num_corre_cpe_ref) as nro from ospos_factura f `;
	if(params.hasOwnProperty('serie')){
		doc.query += `where f.cod_tip_otr_doc_ref = '`+ params.serie+`'`;
	}
	return doc.query;
};

module.exports = doc;