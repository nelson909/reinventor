var express = require('express');
var router = express.Router();

var order = {
	query : '',
	insert : '',
	update : ''
};

order.getQuery = function(params){
	order.query = '';
	order.query += `select 
					o.id,
					o.date_order,
					o.reception_date_order,
					o.deleted,
					o.state_order_id,
					o.concept_id,
					o.entity_id,
					o.employee_id,
					o.created_at,
					i.name as item_name,
					i.category as item_category,
					i.item_number as item_number,
					i.description as item_description,
					i.cost_price as item_cost,
					i.unit_price as item_unit_price,
					i.quantity as item_quantity,
					i.reorder_level as item_level,
					i.location as item_location,
					i.item_id as item_allow,
					i.allow_alt_description as item_allow_description,
					i.is_serialized as item_serialize
				from ospos_orders_detail od
				inner join ospos_orders o on od.order_id = o.id
				inner join ospos_items i on od.product_id = i.item_id
				WHERE o.deleted = 0 and o.state_order_id = 10 `;
		if(Object.keys(params).length !== 0){
			if(params.hasOwnProperty('id')){
				order.query += `and o.id = `+ params.id;
			}
		}
	order.query += ` ORDER BY o.id DESC`;
	return order.query;
};