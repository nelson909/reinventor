var express = require('express');
var router = express.Router();
var sm = require('sitemap');

var sitemap = sm.createSitemap ({
	hostname: 'http://example.com',
	cacheTime: 600000,
	urls: [
		{ url: '/page-1/', changefreq: 'daily', priority: 0.3 },
		{ url: '/page-2/', changefreq: 'monthly',  priority: 0.7 },
		{ url: '/page-3/'},
		{ url: '/page-4/',   img: "http://urlTest.com" }
	]
});

/* GET home page. */
router.get('/', function(req, res, next) {
	/*
	sitemap.toXML( function (err, xml) {
		console.log(">>>>>"+err);
		if (err) {
			return res.status(500).end();
		}
		res.header('Content-Type', 'application/xml');
		res.send(xml);
  	});
  	*/
  res.render('index', { title: 'Express' });
});

module.exports = router;
